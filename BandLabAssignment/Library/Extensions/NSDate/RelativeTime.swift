//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation

class RelativeTime {
    
    class func createRelativeTimeString(createdDate: String) -> String {
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(identifier: "UTC")
        
        if let date = dateFormatter.date(from: createdDate) {
            return self.timeAgoSince(date)
        }
        return ""
    }

    class func calculateRelativeTime(time: Int) -> String {
        return timeAgoSince(NSDate(timeIntervalSince1970: TimeInterval(time)) as Date)
    }

    class func timeAgoSince(_ date: Date) -> String {

        let calendar = Calendar.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
        let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])

        if let year = components.year, year >= 2 {
            return "\(year) \(RelativeTimeIdentifier.timeAgoText.yearsAgo.rawValue)"
        }

        if let year = components.year, year >= 1 {
            return "\(year) \(RelativeTimeIdentifier.timeAgoText.yearAgo.rawValue)"
        }

        if let month = components.month, month >= 2 {
            return "\(month) \(RelativeTimeIdentifier.timeAgoText.monthsAgo.rawValue)"
        }

        if let month = components.month, month >= 1 {
            return "\(month) \(RelativeTimeIdentifier.timeAgoText.monthAgo.rawValue)"
        }

        if let week = components.weekOfYear, week >= 2 {
            return "\(week) \(RelativeTimeIdentifier.timeAgoText.weeksAgo.rawValue)"
        }

        if let week = components.weekOfYear, week >= 1 {
            return "\(week) \(RelativeTimeIdentifier.timeAgoText.weekAgo.rawValue)"
        }

        if let day = components.day, day >= 2 {
            return "\(day) \(RelativeTimeIdentifier.timeAgoText.daysAgo.rawValue)"
        }

        if let day = components.day, day >= 1 {
            return "\(day) \(RelativeTimeIdentifier.timeAgoText.dayAgo.rawValue)"
        }

        if let hour = components.hour, hour >= 2 {
            return "\(hour) \(RelativeTimeIdentifier.timeAgoText.hoursAgo.rawValue)"
        }

        if let hour = components.hour, hour >= 1 {
            return "\(hour) \(RelativeTimeIdentifier.timeAgoText.hourAgo.rawValue)"
        }

        if let minute = components.minute, minute >= 2 {
            return "\(minute) \(RelativeTimeIdentifier.timeAgoText.minutesAgo.rawValue)"
        }

        if let minute = components.minute, minute >= 1 {
            return "\(minute) \(RelativeTimeIdentifier.timeAgoText.minuteAgo.rawValue)"
        }

        if let second = components.second, second >= 3 {
            return "\(second) \(RelativeTimeIdentifier.timeAgoText.secondsAgo.rawValue)"
        }

        return RelativeTimeIdentifier.timeAgoText.justAgo.rawValue

    }

}


//MARK: - RelativeTime Identifier Constants -
enum RelativeTimeIdentifier {
    enum timeAgoText: String {
        case justAgo, secondsAgo, minuteAgo, minutesAgo, hourAgo, hoursAgo, dayAgo, daysAgo, weekAgo, weeksAgo, monthAgo, monthsAgo, yearAgo, yearsAgo
        var rawValue: String {
            get {
                switch self {
                case timeAgoText.justAgo:
                    return NSLocalizedString("Just now", comment: "justAgo")
                case timeAgoText.secondsAgo:
                    return NSLocalizedString("seconds ago", comment: "secondsAgo")
                case timeAgoText.minuteAgo:
                    return NSLocalizedString("minute ago", comment: "minuteAgo")
                case timeAgoText.minutesAgo:
                    return NSLocalizedString("minutes ago", comment: "minutesAgo")
                case timeAgoText.hourAgo:
                    return NSLocalizedString("hour ago", comment: "hourAgo")
                case timeAgoText.hoursAgo:
                    return NSLocalizedString("hours ago", comment: "hoursAgo")
                case timeAgoText.dayAgo:
                    return NSLocalizedString("day ago", comment: "dayAgo")
                case timeAgoText.daysAgo:
                    return NSLocalizedString("days ago", comment: "daysAgo")
                case timeAgoText.weekAgo:
                    return NSLocalizedString("week ago", comment: "weekAgo")
                case timeAgoText.weeksAgo:
                    return NSLocalizedString("weeks ago", comment: "weeksAgo")
                case timeAgoText.monthAgo:
                    return NSLocalizedString("month ago", comment: "monthAgo")
                case timeAgoText.monthsAgo:
                    return NSLocalizedString("months ago", comment: "monthsAgo")
                case timeAgoText.yearAgo:
                    return NSLocalizedString("year ago", comment: "yearAgo")
                case timeAgoText.yearsAgo:
                    return NSLocalizedString("years ago", comment: "yearsAgo")
                }
            }
        }
    }
}
