//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension MusicPlayerManager {
    
    func downloadSong(song: SongData?) {
        
        guard song?.audioURL != nil else {
            return
        }
        
        
        let url = URL(string: (song?.audioURL!)!)
        guard url != nil else {
            return
        }

        self.saveDownloadedFileDetailsInLocalDB(song: song!) // Local Database

        let blDownloader = BLDownloadManager.sharedInstance

        let progressClosure : (CGFloat,BLDownloadInstance) -> (Void)
        let remainingTimeClosure : (CGFloat) -> Void
        let completionClosure : (Bool, String?, Error?) -> Void


        progressClosure = {(progress,downloadInstace) in
            print("Progress of File : \(downloadInstace.filename) is \(Float(progress))")
            NotificationCenter.default.post(name: .DownloadProgressNotification, object: self, userInfo: ["fileName": downloadInstace.filename, "progress": progress])
        }

        remainingTimeClosure = {(timeRemaning) in
            print("Remaining Time is : \(timeRemaning)")
        }

        completionClosure = {(status, message, error) in
            print("is Download completed : \(status)")
            print("Download status message : \(message ?? "")")
            if error != nil {
                print("Download error : \(error?.localizedDescription ?? "")")
                if url?.lastPathComponent != nil {
                    _ = CoreDataManager.sharedInstance.deleteOfflineResourceRecord(fileName: (url?.lastPathComponent)!)
                }
                NotificationCenter.default.post(name: .SongDownloadFailedNotification, object: nil, userInfo: ["SongUrl" : url?.absoluteString ?? ""])
                BLDownloadManager.downloadInstanceDictionary?.removeValue(forKey: url?.lastPathComponent ?? "")
            } else {
                _ = CoreDataManager.sharedInstance.updateDownloadStatus(fileName: (url?.lastPathComponent)!)
                NotificationCenter.default.post(name: .DownloadCompletedNotification, object: self, userInfo: ["SongUrl" : url?.absoluteString ?? ""])
//                self.extractFile()
            }
        }

        blDownloader.downloadFile(
            withURL: url!,
            name: url!.lastPathComponent,
            directoryName: nil,
            friendlyName: nil,
            progressClosure: progressClosure,
            remainigtTimeClosure: remainingTimeClosure,
            completionClosure: completionClosure,
            backgroundingMode: true)
    }
    
    func loadFileAfterDownloading(videoPath:String){
        
    }
    
    func extractFile() {
        do{
            let cachesDirectoryURLPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
            
            let dir = try FileManager.default.contentsOfDirectory(atPath: cachesDirectoryURLPath.path)
            for file in dir{
                print("file is : \(file)")
            }
            
        }
        catch let error as NSError{
            print("error occured while trying to read cache dir \(error.localizedDescription)")
        }
    }
    
    func saveDownloadedFileDetailsInLocalDB(song:SongData) {
        
        var offlineResourceDetails: [String:AnyObject] = [:]
        offlineResourceDetails["name"] = song.name as AnyObject
        offlineResourceDetails["downloadUrl"] = song.audioURL as AnyObject
        offlineResourceDetails["jsonString"] = convertDictionaryToJSONString(dict: song.dictionaryRepresentation() as [String : AnyObject]) as AnyObject
        offlineResourceDetails["isFileDownloadCompleted"] = false as AnyObject
        
        if CoreDataManager.sharedInstance.saveOfflineResourceRecord(offlineResourceDetails: offlineResourceDetails) == false {
            print("Failed to store details in local DB.")
        }
    }
    
    func clearOffileData() {
        self.player.pause()
        CoreDataManager.sharedInstance.deleteAll()
        FileManager.default.deleteAllFiles()
        
    }
}
