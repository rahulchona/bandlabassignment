//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import CoreGraphics
import UserNotifications
import UIKit


public protocol DownloadManagerDelegate {
    func downloadItemAdded(downloadInstance : BLDownloadInstance)
    func downloadItemRemoved(downloadInstance : BLDownloadInstance)
    
}

public class BLDownloadManager : NSObject,URLSessionDelegate,URLSessionDownloadDelegate,UNUserNotificationCenterDelegate
    
{
    
    public var currentDownloads : Array<String>?
    public var backgroundTransferCompletionHandler :( () -> ())?
    public var session : URLSession?
    public var backgroundSession : URLSession?
    public  static var downloadInstanceDictionary : Dictionary<String, BLDownloadInstance>?
    
    public var delegate : DownloadManagerDelegate?
    
    public static let sharedInstance = BLDownloadManager()
    
    override private init() {
        
        super.init()
        let configuration = URLSessionConfiguration.default
        self.session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        let backgroundConfiguration  = URLSessionConfiguration.background(withIdentifier: "com.rahulchona.BLDownloader")
        self.backgroundSession = URLSession(configuration: backgroundConfiguration, delegate: self, delegateQueue: nil)
        self.currentDownloads = Array<String>()
        BLCacheManagement.cleanTempDirectory()
        UNUserNotificationCenter.current().delegate = self
        
    }
    
    
    public func downloadFileWithBLDownloader(
        withURL: URL,
        name : String,
        directoryName:String?,
        friendlyName : String?,
        backgroundingMode :Bool
        
        ) -> Void {
        
        if(BLDownloadManager.downloadInstanceDictionary == nil)
        {
            BLDownloadManager.downloadInstanceDictionary = Dictionary<String,BLDownloadInstance>()
        }
        
        if(BLCacheManagement.fileExistForURL(url: withURL))
        {
            let result =  BLCacheManagement.deleteFileForURL(url: withURL)
            print("file1 got deleted : \(result)")
        }
        
        //Do the Session networking here
        var fileName  = name
        let url  = withURL
        var friendlyName = friendlyName
        var directoryName = directoryName
        
        if (fileName.isEmpty) {
            fileName = withURL.lastPathComponent
        }
        
        if (friendlyName == nil) {
            friendlyName = fileName;
        }
        
        if (directoryName == nil)
        {
            directoryName = BLCacheManagement.cachesDirectoryURlPath().appendingPathComponent(name).absoluteString
        }
        
        if(BLCacheManagement.fileDownloadCompletedForURL(url: url))
        {
            print("file is finished downloading")
        }
        else if(!(BLCacheManagement.fileExistForURL(url: url, directory: directoryName)))
        {
            
            let request = URLRequest(url: url)
            let downloadTask : URLSessionDownloadTask
            if(backgroundingMode)
            {
                downloadTask = self.backgroundSession!.downloadTask(with: request)
            }
            else
            {
                downloadTask  = self.session!.downloadTask(with: request)
            }
            
            let downloadInstance = BLDownloadInstance(withDownloadTask: downloadTask, remainingTime: nil, progess: nil, status: nil, name: fileName, friendlyName: friendlyName!, path: directoryName!, date: Date())
            
            BLDownloadManager.downloadInstanceDictionary![url.lastPathComponent] = downloadInstance
            if(delegate != nil)
            {
                delegate?.downloadItemAdded(downloadInstance: downloadInstance)
            }
            
            downloadTask.resume()
            
            
        }
        else
        {
            print("file already exists")
            
        }
        
    }
    
    public func downloadFile(
        withURL: URL,
        name : String,
        directoryName:String?,
        friendlyName : String?,
        progressClosure : @escaping ((CGFloat,BLDownloadInstance)->(Void)),
        remainigtTimeClosure : @escaping ((CGFloat)->(Void)),
        completionClosure : @escaping ((Bool, String?, Error?)->(Void)),
        backgroundingMode :Bool
        
        ) -> Void {
        
        if(BLDownloadManager.downloadInstanceDictionary == nil)
        {
            BLDownloadManager.downloadInstanceDictionary = Dictionary<String,BLDownloadInstance>()
        }
        
        if(BLCacheManagement.fileDownloadCompletedForURL(url: withURL))
        {
            print("file is finished downloading")
            if(BLCacheManagement.fileExistForURL(url: withURL))
            {
                let result =  BLCacheManagement.deleteFileForURL(url: withURL)
                print("file1 got deleted : \(result)")
            }
            
        }
        
        
        //Do the Session networking here
        var fileName  = name
        let url  = withURL
        var friendlyName = friendlyName
        var directoryName = directoryName
        
        if (fileName.isEmpty) {
            fileName = withURL.lastPathComponent
        }
        
        if (friendlyName == nil) {
            friendlyName = fileName;
        }
        
        if (directoryName == nil)
        {
            directoryName = BLCacheManagement.cachesDirectoryURlPath().appendingPathComponent(name).absoluteString
        }
        
        if(BLCacheManagement.fileDownloadCompletedForURL(url: url))
        {
            print("file is finished downloading")
        }
        else if(!(BLCacheManagement.fileExistForURL(url: url, directory: directoryName)))
        {
            
            let request = URLRequest(url: url)
            let downloadTask : URLSessionDownloadTask
            if(backgroundingMode)
            {
                downloadTask = self.backgroundSession!.downloadTask(with: request)
            }
            else
            {
                downloadTask  = self.session!.downloadTask(with: request)
            }
            
            let downloadInstance = BLDownloadInstance(withDownloadTask: downloadTask, remainingTime: remainigtTimeClosure, progess: progressClosure, status: completionClosure, name: fileName, friendlyName: friendlyName!, path: directoryName!, date: Date())
            
            BLDownloadManager.downloadInstanceDictionary![url.lastPathComponent] = downloadInstance
            
            
            downloadTask.resume()
            
            
        }
        else
        {
            print("file already exists")
            
            //print("data written \(totalBytesWritten)")
//            let fileIdentifier  = url.lastPathComponent
//            let dowloadInstace = BLDownloadManager.downloadInstanceDictionary![fileIdentifier]
            
            DispatchQueue.main.async {
                completionClosure(true, "File already exists:\(url.lastPathComponent)" , nil)
            }
        }
        
    }
    
    
    
    //MARK : Session
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        if(BLDownloadManager.downloadInstanceDictionary == nil)
        {
            BLDownloadManager.downloadInstanceDictionary = Dictionary<String,BLDownloadInstance>()
        }
        
        //print("data written \(totalBytesWritten)")
        let fileIdentifier  = downloadTask.originalRequest!.url!.lastPathComponent
        let dowloadInstace = BLDownloadManager.downloadInstanceDictionary![fileIdentifier]
        guard dowloadInstace != nil else {
            dowloadInstace?.downloadStatusClosure!(false, "Something went wrong" , NSError(domain: "Something went wrong", code: 400, userInfo: nil))
            return
        }
        let  progress = CGFloat.init(totalBytesWritten) / CGFloat.init(totalBytesExpectedToWrite)
        debugPrint("progress is   \(progress)")
        print("progress is   \(progress)")
        
        DispatchQueue.main.async {
            dowloadInstace!.currentProgressClosure!(CGFloat(progress),dowloadInstace!)
        }
        
        let remainingTime = self.remainingTimeForDownload(downloadInstace: dowloadInstace!, totalBytesTransferred: totalBytesWritten, totalBytesExpectedToWrite: totalBytesExpectedToWrite)
        
        
        DispatchQueue.main.async {
            dowloadInstace!.remainingTimeClosure! (remainingTime)
        }
    }
    
    public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        
        debugPrint("Download finished: \(location)")
        print("Download finished: \(location)")
        
        if(BLDownloadManager.downloadInstanceDictionary == nil)
        {
            BLDownloadManager.downloadInstanceDictionary = Dictionary<String,BLDownloadInstance>()
        }
        
        let fileIdentifier  = downloadTask.originalRequest!.url!.lastPathComponent
        let dowloadInstace = BLDownloadManager.downloadInstanceDictionary![fileIdentifier]
        guard dowloadInstace != nil else {
            dowloadInstace?.downloadStatusClosure!(false, "Something went wrong" , NSError(domain: "Something went wrong", code: 400, userInfo: nil))
            return
        }
        var destinationLocation : URL  = BLCacheManagement.cachesDirectoryURlPath()
        
        if(dowloadInstace!.filePath.isEmpty){
            destinationLocation = destinationLocation.appendingPathComponent(fileIdentifier)
        }
        else{
            
            destinationLocation = URL(string: dowloadInstace!.filePath)!
        }
        
        var success = true
        
        let response  = downloadTask.response! as! HTTPURLResponse
        let statusCode = response.statusCode
        
        if (statusCode >= 400) {
            success = false
        }
        
        
        if (success) {
            
            
            if(delegate != nil)
            {
                delegate?.downloadItemRemoved(downloadInstance: dowloadInstace!)
            }
            do
            {
                if(BLCacheManagement.fileExistWithName(name: dowloadInstace!.filename))
                    
                {
                    try  FileManager.default.removeItem(at: destinationLocation)
                    
                }
                try  FileManager.default.moveItem(at: location, to: destinationLocation)
                dowloadInstace?.downloadStatusClosure!(true, "Download Completed:\(dowloadInstace!.filename)" , nil)
            }
            catch let error as NSError
            {
                print("error occured \(error.localizedDescription)")
                dowloadInstace?.downloadStatusClosure!(false, error.localizedDescription , error)
            }
            BLDownloadManager.downloadInstanceDictionary!.removeValue(forKey: dowloadInstace!.filename)
            //            dowloadInstace!.downloadStatusClosure!(true)
        } else {
            dowloadInstace?.downloadStatusClosure!(false, "Something went wrong" , NSError(domain: "Something went wrong", code: statusCode, userInfo: nil))
        }
        
        
        //do a local notification
    }
    
    
    
    public func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        debugPrint("Task completed: \(task), error: \(error)")
        print("Task completed: \(task), error: \(error)")
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
        
        guard BLDownloadManager.downloadInstanceDictionary != nil else {
            NotificationCenter.default.post(name: .SongDownloadFailedNotification, object: ["SongUrl" : task.originalRequest?.url])
            return
        }
        
        let fileIdentifier  = task.originalRequest!.url!.lastPathComponent
        let dowloadInstace = BLDownloadManager.downloadInstanceDictionary![fileIdentifier]
        guard dowloadInstace != nil else {
            dowloadInstace?.downloadStatusClosure!(false, "Something went wrong" , NSError(domain: "Something went wrong", code: 400, userInfo: nil))
            return
        }
        dowloadInstace?.downloadStatusClosure!(false, error?.localizedDescription , error)
        
        NotificationCenter.default.post(name: .SongDownloadFailedNotification, object: ["SongUrl" : task.originalRequest?.url])
    }
    
    
    public func remainingTimeForDownload(downloadInstace : BLDownloadInstance, totalBytesTransferred : Int64,totalBytesExpectedToWrite : Int64) -> CGFloat {
        
        let timeInterval : TimeInterval  = Date().timeIntervalSince(downloadInstace.downloadDate)
        
        let speed  = CGFloat.init(totalBytesTransferred) / CGFloat.init(timeInterval)
        let remainingBytes = totalBytesExpectedToWrite - totalBytesTransferred
        let remainingTime = CGFloat.init(remainingBytes) / CGFloat.init(speed)
        return  remainingTime
    }
    
    
    
    
    
    
    
    //MARK : Finish Session
    
    public func urlSessionDidFinishEvents(forBackgroundURLSession session: URLSession) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        
        session.getAllTasks { (taskArray) in
            if(taskArray.count == 0)
            {
                if(self.backgroundTransferCompletionHandler != nil)
                {
                    let completer = self.backgroundTransferCompletionHandler
                    
                    OperationQueue().addOperation(
                        completer!
                    )
                    
                    self.backgroundTransferCompletionHandler = nil;
                    
                }
            }
        }
    }
    
    
    
    
    
}
