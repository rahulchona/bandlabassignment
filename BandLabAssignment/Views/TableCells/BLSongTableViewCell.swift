//
//  SongTableViewCell.swift
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import UIKit

// MARK: - UpdateSongList Protocol -
protocol UpdateSongListDelegate {
    func reloadSongListTableView()
}

class BLSongTableViewCell: UITableViewCell {

    // MARK: - Variables -
    var songData: SongData?
    public var delegate : UpdateSongListDelegate?
    
    // MARK: - IBOutlets -
    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var songActionsButton: UIButton!
    @IBOutlet weak var circleProgressView: CircleProgressBarView!
    
    //MARK: - Initialize cell -
    class func songTableViewCellObject(forTable tableView: UITableView, indexPath: IndexPath) -> BLSongTableViewCell {
        let cell: BLSongTableViewCell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier(), for: indexPath) as! BLSongTableViewCell
        return cell
    }
    
    class func registerNib(forTable tableView: UITableView) {
        let BLSongTableViewCellNib: UINib = UINib(nibName: "BLSongTableViewCell", bundle: nil)
        tableView.register(BLSongTableViewCellNib, forCellReuseIdentifier: self.cellIdentifier())
    }
    
    class func cellIdentifier() -> String {
        return "BLSongTableViewCellIdentifier"
    }
    
    //MARK: - View life cycle -
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    //MARK: - Configure table view cell -
    func configureCell(songData:SongData) {
        self.songData = songData
        self.songNameLabel.text = songData.name ?? "Song's name not available..."
        setSongActionsButtonImage()
      
        removeObservors()
        setupObservors()
    }
    
    @IBAction func songActionButtonTapped(_ sender: Any) {
        switch (self.songData?.songState ?? SongState.NotDownloaded) {
        case SongState.Downloading:
            //show error that song is being downloading at the moment
            self.songActionsButton.isHidden = true
            self.circleProgressView.isHidden = false
            self.songData?.songState = SongState.Downloading
            break
        case SongState.Playing:
            self.songActionsButton.isHidden = false
            self.circleProgressView.isHidden = true
            playPauseSong()
            break
        case SongState.Paused:
            self.songActionsButton.isHidden = false
            self.circleProgressView.isHidden = true
            playPauseSong()
            break
        case SongState.Downloaded:
            self.songActionsButton.isHidden = false
            self.circleProgressView.isHidden = true
            playPauseSong()
            break
        default:
            self.songActionsButton.isHidden = true
            self.circleProgressView.isHidden = false
            self.songData?.songState = SongState.Downloading
//            self.circleProgressView.animateCircle(duration: 1.0)
            MusicPlayerManager.shared.downloadSong(song: self.songData)
            break
        }
    }
    
}

//MARK: - OTHERS -
extension BLSongTableViewCell {
    func setupObservors() {
        NotificationCenter.default.addObserver(self, selector: #selector(BLSongTableViewCell.downloadProgress(notification:)), name: .DownloadProgressNotification, object: nil)
    }
    
    func removeObservors() {
        NotificationCenter.default.removeObserver(self, name: .DownloadProgressNotification, object: nil)
    }
    
    @objc func downloadProgress(notification: Notification) {
        if (notification.userInfo?["fileName"] as? String) ?? "" == URL(string: (self.songData?.audioURL!)!)?.lastPathComponent {
            DispatchQueue.main.async {
                let progress : CGFloat = ((notification.userInfo?["progress"]) as? CGFloat) ?? 0.0
                self.circleProgressView.progress = progress
            }
        }
    }
    func playPauseSong() {
        
        if MusicPlayerManager.shared.musicData?.song != nil && (MusicPlayerManager.shared.musicData?.song?.count ?? 0) <= MusicPlayerManager.shared.currentSongIndex {
            
            playAnotherSong(isFirstSongPlayed: true)
            
        } else {
            if self.songData?.id != MusicPlayerManager.shared.musicData?.song?[MusicPlayerManager.shared.currentSongIndex].id {
                
                playAnotherSong(isFirstSongPlayed: false)
                
            } else {
                if MusicPlayerManager.shared.player.timeControlStatus == .playing {
                    
                    MusicPlayerManager.shared.player.pause() // Pause the current music track
                    self.songData?.songState = SongState.Paused
                } else if MusicPlayerManager.shared.player.timeControlStatus == .waitingToPlayAtSpecifiedRate {
                    //Do Nothing
                } else if MusicPlayerManager.shared.player.timeControlStatus == .paused {
                    
                    if MusicPlayerManager.shared.player.currentItem != nil {
                        MusicPlayerManager.shared.player.playImmediately(atRate: 1.0) // Play Current Music
                        MusicPlayerManager.shared.musicData?.song?[MusicPlayerManager.shared.currentSongIndex].songState = SongState.Downloaded
                        self.songData?.songState = SongState.Playing
                    } else {
                        MusicPlayerManager.shared.playCurrentSong()
                        MusicPlayerManager.shared.musicData?.song?[MusicPlayerManager.shared.currentSongIndex].songState = SongState.Downloaded
                        self.songData?.songState = SongState.Playing
                    }
                }
            }
        }
        
        setSongActionsButtonImage()
        self.delegate?.reloadSongListTableView()
    }
    
    func playAnotherSong(isFirstSongPlayed: Bool) {
        
        if isFirstSongPlayed == false {
            MusicPlayerManager.shared.musicData?.song?[MusicPlayerManager.shared.currentSongIndex].songState = SongState.Paused
        }
        
        if let song = self.songData {
            MusicPlayerManager.shared.musicData?.song?.removeAll()
            MusicPlayerManager.shared.musicData?.song?.insert(song, at: 0)
            
            MusicPlayerManager.shared.player.removeAllItems()
            MusicPlayerManager.shared.currentSongIndex = 0
            if MusicPlayerManager.shared.playFirstSong == true {
                MusicPlayerManager.shared.playFirstSong = false
            }
            MusicPlayerManager.shared.playCurrentSong()
            self.songData?.songState = SongState.Playing
        }
    }
    
    func setSongActionsButtonImage() {
        switch (self.songData?.songState ?? SongState.NotDownloaded) {
        case SongState.Downloading:
            self.songActionsButton.isHidden = true
            self.circleProgressView.isHidden = false
            break
        case SongState.Playing:
            self.songActionsButton.isHidden = false
            self.circleProgressView.isHidden = true
            self.songActionsButton.setImage(#imageLiteral(resourceName: "play-icon"), for: .normal)
            break
        case SongState.Paused:
            self.songActionsButton.isHidden = false
            self.circleProgressView.isHidden = true
            self.songActionsButton.setImage(#imageLiteral(resourceName: "pause-icon"), for: .normal)
            break
        case SongState.Downloaded:
            self.songActionsButton.isHidden = false
            self.circleProgressView.isHidden = true
            self.songActionsButton.setImage(#imageLiteral(resourceName: "pause-icon"), for: .normal)
            break
        default:
            self.songActionsButton.isHidden = false
            self.circleProgressView.isHidden = true
            self.circleProgressView.progress = 0.0
            self.songActionsButton.setImage(#imageLiteral(resourceName: "download-icon"), for: .normal)
        }
    }
}
