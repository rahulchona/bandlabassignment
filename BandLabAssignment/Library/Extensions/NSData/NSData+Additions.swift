//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation

// MARK: - NSData Extension
extension Data {

    func json() -> AnyObject? {

        var object: AnyObject?

        do {
            object = try JSONSerialization.jsonObject(with: self, options: []) as? [String: AnyObject] as AnyObject
            // use anyObj here
        } catch {
            print("json error: \(error)")
        }

        return object
    }

    /**
     Get base 64 strinf from nsdata

     - returns: A NSString
     */
    func toBase64EncodedString() -> String {

        return base64EncodedString(options: [NSData.Base64EncodingOptions()])
    }
}
