//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIView Extension
extension UIView {

    // - MARK: - Loading Progress View
    func showLoader(subTitle subtitle: String?) {
    }

    func hideLoader() {
    }
}
