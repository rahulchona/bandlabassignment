# BandLabAssignment

## Build Requirements
+ Xcode 9.0 or later
+ iOS 10.0 SDK or later
+ CocoaPods

## Runtime Requirements
+ iOS 10.0 or later

## About Project

Project summary

### Builds

## Version #1.0

## Build #1
Songs List Screen
Downloading Songs Functionality
Downloaded Songs Listing in Coredata
Informatiom Screen
    - Showing Total space occupied by the app
    - Clear Offile Data

## Important Modules
MusicPlayerManager
Download Engine
CoredataManager
NetworkManager
DataManager
CustomViews - ProgressBar
ViewControllers Folder
Views Folder

## Important Note
Songs list is extracted out of the "Songs.json" file, you may find that file in Bundle.
