//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import UIKit

// MARK: - UIFont Extension
extension UIFont {
    func helvaticaNewBold(_ size: CGFloat) -> UIFont {
        return UIFont(name: "helvetica", size: size)!
    }

    //Montserrat-Medium
    class func montserratMedium(_ size: CGFloat) -> UIFont {
        if let font: UIFont = UIFont(name: "Montserrat-Medium", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    //Montserrat-SemiBold
    class func montserratSemiBold(_ size: CGFloat) -> UIFont {
        if let font: UIFont = UIFont(name: "Montserrat-SemiBold", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    //Montserrat-Regular
    class func montserratRegular(_ size: CGFloat) -> UIFont {
        if let font: UIFont = UIFont(name: "Montserrat-Regular", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    //Montserrat-Light
    class func montserratLight(_ size: CGFloat) -> UIFont {
        if let font: UIFont = UIFont(name: "Montserrat-Light", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }

    //Brush Script
    class func brushScript(_ size: CGFloat) -> UIFont {
        if let font: UIFont = UIFont(name: "Brush Script", size: size) {
            return font
        }
        return UIFont.systemFont(ofSize: size)
    }
}
