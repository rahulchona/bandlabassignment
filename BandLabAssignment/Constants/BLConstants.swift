//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate

//**************************************
// GENERAL SETTINGS
//**************************************

// Display Comments
let kDebugLog = true

struct BLConstants {

    // MARK: - UIScreen Constants -
    static let DEFAULT_SCREEN_RATIO: CGFloat = 375.0
    static let KSCREEN_WIDTH: CGFloat = UIScreen.main.bounds.size.width
    static let KSCREEN_HEIGHT: CGFloat = UIScreen.main.bounds.size.height
    static let KSCREEN_WIDTH_RATIO: CGFloat = UIScreen.main.bounds.size.width / 375.0
    static let KSCREEN_HEIGHT_RATIO: CGFloat = UIScreen.main.bounds.size.height / 667.0
    static let SCREEN_MAX_LENGTH    = max(BLConstants.KSCREEN_WIDTH, BLConstants.KSCREEN_HEIGHT)
    static let SCREEN_MIN_LENGTH    = min(BLConstants.KSCREEN_WIDTH, BLConstants.KSCREEN_HEIGHT)
    static let DEFAULT_SCREEN_RATIO_HEIGHT_CALCULATED: CGFloat = KSCREEN_HEIGHT / 667.0

    struct deviceType {
        static let IS_IPHONE_5_OR_LESS = UIDevice.current.userInterfaceIdiom == .phone && BLConstants.SCREEN_MAX_LENGTH <= 568
        static let IS_IPHONE_8_OR_LESS = UIDevice.current.userInterfaceIdiom == .phone && BLConstants.SCREEN_MAX_LENGTH <= 667
        static let IS_IPHONE_4 = UIDevice.current.userInterfaceIdiom == .phone && BLConstants.SCREEN_MAX_LENGTH == 420
        static let IS_IPHONE_5 = UIDevice.current.userInterfaceIdiom == .phone && BLConstants.SCREEN_MAX_LENGTH == 568
        static let IS_IPHONE_8 = UIDevice.current.userInterfaceIdiom == .phone && BLConstants.SCREEN_MAX_LENGTH == 667
        static let IS_IPHONE_8P = UIDevice.current.userInterfaceIdiom == .phone && BLConstants.SCREEN_MAX_LENGTH == 736
        static let IS_IPHONE_X = UIDevice.current.userInterfaceIdiom == .phone && BLConstants.SCREEN_MAX_LENGTH == 812
        static let IS_IPAD = UIDevice.current.userInterfaceIdiom == .pad && BLConstants.SCREEN_MAX_LENGTH == 1024
    }
}
