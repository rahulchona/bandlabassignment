//
//  SongListViewController+TableViewDelegates.swift
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

extension BLSongListViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view delegate -
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.songsBase?.data?.count) ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let songTableViewCell: BLSongTableViewCell = BLSongTableViewCell.songTableViewCellObject(forTable: tableView, indexPath: indexPath)
        songTableViewCell.configureCell(songData: (self.songsBase?.data?[indexPath.row])!)
        songTableViewCell.delegate = self
        return songTableViewCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let songUrl = URL(string: ((self.songsBase?.data?[indexPath.row])?.audioURL!)!) {

            if BLCacheManagement.fileExistForURL(url: songUrl) {
                (tableView.cellForRow(at: indexPath) as? BLSongTableViewCell)?.playPauseSong()
            }
        }
    }
}

