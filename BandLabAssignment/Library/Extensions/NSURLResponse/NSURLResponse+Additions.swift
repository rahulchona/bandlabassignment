//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation

// MARK: - NSURLResponse Extension
extension URLResponse {

    func isHTTPResponseValid() -> Bool {

        if let response = self as? HTTPURLResponse {
            return response.statusCode >= 200 && response.statusCode <= 299
        }
        return false
    }
}
