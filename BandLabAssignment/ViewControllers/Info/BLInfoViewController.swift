//
//  BLInfoViewController.swift
//  BandLabAssignment
//
//  Created by Rahul Chona on 23/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import UIKit

class BLInfoViewController: UIViewController {
    
    //MARK: - IBOUTLETS -
    @IBOutlet weak var occupiedSpaceLabel: UILabel!
    
    //MARK: - VIEWCONTROLLER LIFECYCLE METHODS -
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getDocumentDirectSpaceOccupied()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - ACTIONS -
    @IBAction func close(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearOfflineData(_ sender: Any) {
        stopAllDownloadTasks()
        MusicPlayerManager.shared.clearOffileData()
        NotificationCenter.default.post(name: .ReprocessSongsStateNotification, object: nil)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - STORAGE RELATED -
extension BLInfoViewController {
    private func getDocumentDirectSpaceOccupied() {
        if FileManager.documentsDirectoryPath() != nil {
            self.occupiedSpaceLabel.text = FileManager.default.sizeOfFolder(FileManager.documentsDirectoryPath() ?? "") ?? "N/A"
        }
    }
    
    private func stopAllDownloadTasks() {
        BLDownloadManager.sharedInstance.backgroundSession?.getAllTasks(completionHandler: { (allDownloadTasks) in
            for downloadTask in allDownloadTasks {
                downloadTask.cancel()
            }
        })
        BLDownloadManager.downloadInstanceDictionary = nil
    }
}
