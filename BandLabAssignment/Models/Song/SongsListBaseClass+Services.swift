//
//  SongsListBaseClass+Services.swift
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import ObjectMapper

extension DataManager {
    
    // MARK: - Songs list API -
    func getSongsListAPI(completion: @escaping(_ success: Bool, _ error: Error?, _ message: String, _ songs: SongsListBaseClass?) -> Void) {
        
        if let songsBaseDict = BLGenericMethods.readJson() {
            
            guard songsBaseDict as? [String:AnyObject] != nil else {
                completion(false, NSError(domain:"Invalid Json or File not found", code:9999, userInfo:nil), "Something went wrong", nil)
                return
            }
            
            if let songsBaseModel: SongsListBaseClass = Mapper<SongsListBaseClass>().map(JSON: songsBaseDict as! [String : Any]) {
                
                self.processSongsWithStates(songsBaseModel: songsBaseModel)
                completion(true, nil, "Success", songsBaseModel)
                
            }
        }
    }
    
    func processSongsWithStates(songsBaseModel : SongsListBaseClass) {
        
        guard (songsBaseModel.data?.count ?? 0) > 0 else {
            return
        }
        
        for song in songsBaseModel.data! {

            if song.audioURL != nil && URL(string: (song.audioURL!)) != nil {
                
                if BLCacheManagement.fileExistForURL(url: URL(string: (song.audioURL!))!) {
                    song.songState = SongState.Downloaded
                }
            }
        }
    }
    
    func reprocessSongsWithStates(songsBaseModel : SongsListBaseClass) {
        
        guard (songsBaseModel.data?.count ?? 0) > 0 else {
            return
        }
        
        for song in songsBaseModel.data! {
            
            if song.audioURL != nil && URL(string: (song.audioURL!)) != nil {
                
                if BLCacheManagement.fileExistForURL(url: URL(string: (song.audioURL!))!) {
                    song.songState = SongState.Downloaded
                } else {
                    song.songState = SongState.NotDownloaded
                }
            }
        }
    }
}
