//
//  BandLabGlobal
//
//  Created by Rahul Chona  on 27/01/17.
//  Copyright © 2017 Rahul Chona . All rights reserved.
//

import Foundation

func convertJSONStringToDictionary(jsonString: String) -> [String:AnyObject]? {
    if let data = jsonString.data(using: .utf8) {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        } catch {
            print(error.localizedDescription)
        }
    }
    return nil
}


func convertDictionaryToJSONString(dict: [String : AnyObject]) -> String? {
    if let theJSONData = try? JSONSerialization.data(
        withJSONObject: dict,
        options: []) {
        return String(data: theJSONData, encoding: .utf8)
    }
    
    return nil
}

