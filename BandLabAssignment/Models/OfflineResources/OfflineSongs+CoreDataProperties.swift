//
//  OfflineSongs+CoreDataProperties.swift
//  
//
//  Created by Rahul Chona on 22/04/18.
//
//

import Foundation
import CoreData


extension OfflineSongs {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<OfflineSongs> {
        return NSFetchRequest<OfflineSongs>(entityName: "OfflineSongs")
    }

    @NSManaged public var jsonString: String?
    @NSManaged public var name: String?
    @NSManaged public var isFileDownloadCompleted: Bool
    @NSManaged public var downloadURL: String?

}
