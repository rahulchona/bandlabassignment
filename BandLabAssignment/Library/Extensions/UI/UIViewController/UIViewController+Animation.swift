//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {

    func flipHorizontalViewContorller(_ viewController: UIViewController!, back: @escaping() -> Void) {
        showViewController(viewController, modalTransitionStyle: UIModalTransitionStyle.flipHorizontal, back: back)
    }

    func coverVerticalViewContorller(_ viewController: UIViewController!, back: @escaping() -> Void) {
        showViewController(viewController, modalTransitionStyle: UIModalTransitionStyle.coverVertical, back: back)
    }

    func crossDissolveViewContorller(_ viewController: UIViewController!, back: @escaping() -> Void) {
        showViewController(viewController, modalTransitionStyle: UIModalTransitionStyle.crossDissolve, back: back)
    }

    func partialCurlViewContorller(_ viewController: UIViewController!, back: @escaping() -> Void) {
        showViewController(viewController, modalTransitionStyle: UIModalTransitionStyle.partialCurl, back: back)
    }

    func showViewController(_ viewController: UIViewController!, modalTransitionStyle: UIModalTransitionStyle, back: @escaping() -> Void) {
        viewController.modalTransitionStyle = modalTransitionStyle
        present(viewController, animated: true, completion: back)
    }
}
