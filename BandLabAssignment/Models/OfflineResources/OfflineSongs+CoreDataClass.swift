//
//  OfflineSongs+CoreDataClass.swift
//
//
//  Created by Rahul Chona on 22/04/18.
//
//

import Foundation
import CoreData
import UIKit

@objc(OfflineSongs)
public class OfflineSongs: NSManagedObject {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    class func saveOfflineResource(offlineResourceDetails: [String:AnyObject]) -> Bool {
        
        //check if details already exist then just update
        if self.checkIfOfflineResourceAlreadyExist(offlineResource: offlineResourceDetails["name"] as! String) { //As name is unique
            
            return updateOfflineResource(offlineResourceDetails:offlineResourceDetails)
        } else {
            //create new contact
            return insertOfflineResource(offlineResourceDetails:offlineResourceDetails)
        }
    }
    
    class func checkIfOfflineResourceAlreadyExist(offlineResource:String) -> Bool {
        //FIXME: IMPLEMENT
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        let predicate = NSPredicate(format: "name == %@", offlineResource)
        request.predicate = predicate
        //        request.fetchLimit = 1
        
        do {
            let count = try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.count(for: request)
            if  count == NSNotFound {
                return false
            } else if count == 0 {
                // no matching object
                return false
            } else {
                // at least one matching object exists
                return true
            }
        }
        catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    class func insertOfflineResource(offlineResourceDetails:[String:AnyObject]) -> Bool {
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let offlineResource = OfflineSongs(entity: NSEntityDescription.entity(forEntityName: "OfflineSongs", in: context)!, insertInto: context)
        
        //        let offlineResources = OfflineSongs(context: CoreDataManager.sharedInstance.context!) // Link Task & Context
        
        offlineResource.name = offlineResourceDetails["name"] as! String?
        offlineResource.jsonString = offlineResourceDetails["jsonString"] as! String?
        offlineResource.downloadURL = offlineResourceDetails["downloadUrl"] as! String?
        
        // Save the data to coredata
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        return true
    }
    
    class func updateOfflineResource(offlineResourceDetails : [String:AnyObject]) -> Bool {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        let predicate = NSPredicate(format: "name == %@", (offlineResourceDetails["name"] as! String?)!)
        request.predicate = predicate
        //        request.fetchLimit = 1
        
        do {
            if let fetchResults = try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.fetch(request) as? [NSManagedObject] {
                if fetchResults.count != 0 {
                    
                    let managedObject = fetchResults[0]
                    managedObject.setValue(offlineResourceDetails["jsonString"] as! String?, forKey: "jsonString")
                    
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    return true
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    class func updateDownloadStatus(fileName : String) -> Bool {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        let predicate = NSPredicate(format: "name == %@", fileName)
        request.predicate = predicate
        //        request.fetchLimit = 1
        
        do {
            var fetchResults : [NSManagedObject]?
            DispatchQueue.main.async {
                fetchResults = try! (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.fetch(request) as? [NSManagedObject]
            }
            
            if fetchResults != nil {
                if fetchResults?.count != 0 {
                    
                    let managedObject = fetchResults?[0]
                    managedObject?.setValue(true, forKey: "isFileDownloadCompleted")
                    DispatchQueue.main.async {
                        (UIApplication.shared.delegate as! AppDelegate).saveContext()
                    }
                    return true
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    class func deleteOfflineResource(fileName : String) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        let predicate = NSPredicate(format: "name == %@", fileName)
        request.predicate = predicate
        
        do {
            if let fetchResults = try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.fetch(request) as? [NSManagedObject] {
                if fetchResults.count > 0 {
                    
                    for object in fetchResults {
                        (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.delete(object)
                    }
                    
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    
    class func deleteOfflineResource(offlineResourceDetails : [String:AnyObject]) -> Bool {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        let predicate = NSPredicate(format: "name == %@", (offlineResourceDetails["name"] as! String?)!)
        request.predicate = predicate
        
        do {
            if let fetchResults = try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.fetch(request) as? [NSManagedObject] {
                if fetchResults.count > 0 {
                    
                    for object in fetchResults {
                        (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.delete(object)
                    }
                    
                    (UIApplication.shared.delegate as! AppDelegate).saveContext()
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return false
    }
    
    class func deleteAllOfflineResourceRecords() {
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        let request = NSBatchDeleteRequest(fetchRequest: fetch)
        do {
            try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.execute(request)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
        } catch let error as NSError {
            print("Could not delete all records \(error), \(error.userInfo)")
        }
    }
    
    class func fetchOfflineResourceByName(offlineResourceName : String) -> OfflineSongs? {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        let predicate = NSPredicate(format: "name == %@", offlineResourceName)
        request.predicate = predicate
        //        request.fetchLimit = 1
        
        do {
            if let fetchResults = try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.fetch(request) as? [NSManagedObject] {
                if fetchResults.count != 0 {
                    
                    return fetchResults[0] as? OfflineSongs
                    
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
    }
    
    class func fetchSongsOfflineResources() -> [Any]? {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        do {
            if let fetchResults = try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext.fetch(request) as? [NSManagedObject] {
                if fetchResults.count > 0 {
                    return fetchResults
                }
            }
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
        }
        return nil
    }
    
}


