//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import AVKit
import AVFoundation
import MediaPlayer

class MusicPlayerManager : NSObject {
    
    //MARK: VARIABLES
    var musicData : MusicData? = MusicData()
    var isplayingVideo : Bool = false
    var currentSongIndex = 0
    var playFirstSong = false
    var previousRandomIndex = -1
    //MARK: LOCAL VARIABLES
    var player : AVQueuePlayer = AVQueuePlayer()
    var playerItem : AVPlayerItem?
    var currentSongUrl : String?
    var currentSongPoster : String?
    
    var isRepeatOn : Bool? = false
    var isShuffleOn : Bool? = false
    
    var isProcessingRequest : Bool? = false
    var deletedCurrentPlayingSongFromQueue  : Bool? = false
    // MARK: Shared Instance
    static let shared = MusicPlayerManager()
    
    // Can't init is singleton
    private override init() {
        super.init()
        setupManager()
    }
    
    func sharedInstance() -> MusicPlayerManager {
        return .shared
    }
    
    func setupManager() { // Call only once when app launches
        
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            print("AVAudioSession Category Playback OK")
            do {
                try AVAudioSession.sharedInstance().setActive(true)
                print("AVAudioSession is Active")
            } catch let error as NSError {
                print(error.localizedDescription)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        UIApplication.shared.beginReceivingRemoteControlEvents()
        
        let commandCenter = MPRemoteCommandCenter.shared()
        commandCenter.nextTrackCommand.isEnabled = true
        commandCenter.previousTrackCommand.isEnabled = true
        commandCenter.playCommand.isEnabled = true
        commandCenter.pauseCommand.isEnabled = true
        commandCenter.stopCommand.isEnabled = true
        commandCenter.changeRepeatModeCommand.isEnabled = true
        
        commandCenter.nextTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in

            if self.isProcessingRequest == false {
                if self.isplayingVideo == false {
                    
                    if self.playFirstSong == true {
                        self.playFirstSong = false
                        if (self.musicData?.song?.count)! > self.currentSongIndex {
                            self.performSelector(onMainThread: #selector(self.playCurrentSong), with: nil, waitUntilDone: true)
                        }
                    } else if (self.musicData?.song?.count)! > self.currentSongIndex + 1 {
                        self.currentSongIndex = self.currentSongIndex + 1
                        self.performSelector(onMainThread: #selector(self.playCurrentSong), with: nil, waitUntilDone: true)
                    }
                }
            } else {
                debugPrint("process current request")
            }
            return .success
        }
        
        commandCenter.previousTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            if self.isProcessingRequest == false {
                if self.isplayingVideo == false {
                    if self.playFirstSong == true {
                        self.playFirstSong = false
                        if (self.musicData?.song?.count)! > self.currentSongIndex {
                            self.performSelector(onMainThread: #selector(self.playCurrentSong), with: nil, waitUntilDone: true)
                        }
                    } else if self.currentSongIndex > 0  {
                        self.currentSongIndex = self.currentSongIndex - 1
                        self.performSelector(onMainThread: #selector(self.playCurrentSong), with: nil, waitUntilDone: true)
                    }
                }
            } else {
                debugPrint("process current request")
            }

            return .success
        }
        
        commandCenter.playCommand.addTarget(self, action:#selector(playTrackCommandSelector))
        
        
        commandCenter.pauseCommand.addTarget(self, action:#selector(pauseTrackCommandSelector))
        
        
        commandCenter.stopCommand.addTarget(self, action:#selector(stopTrackCommandSelector))
        
        commandCenter.changeRepeatModeCommand.addTarget(self, action:#selector(repeatTrackCommandSelector))
        
        addObserver() // add observer
        
        player.automaticallyWaitsToMinimizeStalling = false
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = CGRect(x: 0, y: 0, width: 10, height: 50)

        appDelegate.window?.rootViewController?.view.layer.addSublayer(playerLayer)
    }
    
    func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishedPlayTrack), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
    }
    
    func getRandomIndex() -> Int {
        let randomIndex = Int(arc4random_uniform(UInt32((musicData?.song?.count)!)))
        if randomIndex == previousRandomIndex {
            let _ = getRandomIndex()
        } else {
            previousRandomIndex = randomIndex
            return randomIndex
        }
        previousRandomIndex = randomIndex
        return randomIndex
    }
    
    @objc func playerDidFinishedPlayTrack(playerItemNotification: Notification) {
        print("playerDidFinishedPlayTrack")
        
        let stoppedPlayerItem : AVPlayerItem = playerItemNotification.object as! AVPlayerItem
        stoppedPlayerItem.seek(to: kCMTimeZero)
        
        NotificationCenter.default.post(name: .SongFinishedNotification, object: nil, userInfo: ["SongUrl" : self.musicData?.song?[0].audioURL ?? ""])
        
        if isRepeatOn == true {
            if self.playFirstSong == true {
                self.playFirstSong = false
                if (musicData?.song?.count)! > currentSongIndex {
                    playCurrentSong()
                }
            } else if (musicData?.song?.count)! > currentSongIndex {
                playCurrentSong()
            }
            
        } else if isShuffleOn == true {
            
            if self.playFirstSong == true {
                self.playFirstSong = false
                if (musicData?.song?.count)! > currentSongIndex {
                    playCurrentSong()
                }
            } else {
                currentSongIndex = getRandomIndex()
                playCurrentSong()
            }
            
        } else { // linear play next song
            
            if self.playFirstSong == true {
                self.playFirstSong = false
                if (musicData?.song?.count)! > currentSongIndex {
                    playCurrentSong()
                }
            } else if (musicData?.song?.count)! > currentSongIndex + 1 {
                currentSongIndex = currentSongIndex + 1
                playCurrentSong()
            }
            
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        switch (player.status) {
        case .unknown:

            print("playing status: unknown")
        case .readyToPlay:
            
            print("playing status: ready to play")
            
        case .failed:

            print("playing status: failed")
        }
    }
    
    func updatePlayerTimer(duration: Float64)  {
       
    }
    
    func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func nextTrackCommandSelector() {
        debugPrint("Play next track in queue")
        if MusicPlayerManager.shared.player.items().count > 0 {
        if isplayingVideo == false {
            player.currentItem?.seek(to: kCMTimeZero)

            
            if self.playFirstSong == true {
                self.playFirstSong = false
                if (musicData?.song?.count)! > currentSongIndex {
                    playCurrentSong()
                }
            } else if (musicData?.song?.count)! > currentSongIndex + 1 {
                currentSongIndex = currentSongIndex + 1
                playCurrentSong()
            }
        }
        }
    }
    
    @objc func previousTrackCommandSelector() {
        debugPrint("Play previous track in queue")
        if MusicPlayerManager.shared.player.items().count > 0 {
        if isplayingVideo == false {
            player.currentItem?.seek(to: kCMTimeZero)
            
            if self.playFirstSong == true {
                self.playFirstSong = false
                if (musicData?.song?.count)! > currentSongIndex {
                    playCurrentSong()
                }
            } else if currentSongIndex > 0  {
                currentSongIndex = currentSongIndex - 1
                playCurrentSong()
            }
 
        }
        }
    }
    
    @objc func playTrackCommandSelector() {
        debugPrint("Play current track in queue")
        //        player.play()
        if isplayingVideo == false {
            if MusicPlayerManager.shared.player.items().count > 0 {
                if #available(iOS 10.0, *) {
                    player.playImmediately(atRate: 1.0)
                } else {
                    player.play()
                }
        }
        if isplayingVideo == false {
            if MusicPlayerManager.shared.player.items().count > 0 {
                self.setNowPlayingInfoPOster()
            }
            
            updateMPMediaControlCenterDetails()
        }
        }
    }
    
    @objc func pauseTrackCommandSelector() {
        debugPrint("Pause current track in queue")
        if MusicPlayerManager.shared.player.items().count > 0 {
            player.pause()
            if isplayingVideo == false {
                if MusicPlayerManager.shared.player.items().count > 0 {
                    self.setNowPlayingInfoPOster()
                }
                
            }
        }
    }
    
    @objc func stopTrackCommandSelector() {
        debugPrint("Stop track in queue")
        if MusicPlayerManager.shared.player.items().count > 0 {
        removeObserverOfCurrentItem()
        player.pause()
        if isplayingVideo == false {
            player.removeAllItems()
        }
        }
    }
    
    @objc func repeatTrackCommandSelector() {
        print("Repeat track in queue")
    }
    
    func setNowPlayingInfoPOster() {
        if self.isplayingVideo == false {
            if self.musicData?.song != nil, (self.musicData?.song!.count)! > 0 && self.musicData?.song?[self.currentSongIndex].audioURL != nil {
            }
        }
    }
    
    func updateMPMediaControlCenterDetails() {
        if self.isplayingVideo == false {
            if self.musicData?.song != nil, (self.musicData?.song!.count)! > 0 && self.musicData?.song?[self.currentSongIndex].audioURL != nil {
                
                MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle : self.musicData?.song?[self.currentSongIndex].name ?? "Not Available"]

            }
        }
    }
    
    func setNowPlayingInfoCenterDetails() {
        isProcessingRequest = true
        if self.musicData?.song != nil, (self.musicData?.song!.count)! > 0 && self.musicData?.song![self.currentSongIndex].audioURL != nil {
            
            player.pause()
            
            MPNowPlayingInfoCenter.default().nowPlayingInfo = [MPMediaItemPropertyTitle : self.musicData?.song![self.currentSongIndex].name ?? "Not Available"]
            self.startPlayingSong()
        } else {
            isProcessingRequest = false
        }
        
    }
    
    func startPlayingSong() {
        if self.musicData?.song != nil, (self.musicData?.song?.count)! > 0 && self.musicData?.song?[self.currentSongIndex].audioURL != nil && URL(string: (self.musicData?.song![self.currentSongIndex].audioURL!)!) != nil {
            
            player.pause()
            removeObserverOfCurrentItem()
            player.removeAllItems()
            
            let documentsURL = try! FileManager().url(for: .documentDirectory,
                                                      in: .userDomainMask,
                                                      appropriateFor: nil,
                                                      create: true)
            let url:URL = URL(string: (self.musicData?.song![self.currentSongIndex].audioURL!)!)!
            
            let localFileURL = documentsURL.appendingPathComponent(url.lastPathComponent)
            let fileExists = FileManager().fileExists(atPath: localFileURL.path)

            if fileExists == true {
                playerItem = AVPlayerItem(url: localFileURL)
            } else {
                playerItem = AVPlayerItem(url: url)
            }
            
            
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            var commonMetaData = playerItem?.asset.metadata as! [AVMetadataItem]
            for item in commonMetaData {
                print(item.stringValue)
            }
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////////////
            
            
            if player.canInsert(playerItem!, after: nil) {
                player.insert(playerItem!, after: nil)
                
                addObserverToCurrentItem()
                
                if #available(iOS 10.0, *) {
                    player.playImmediately(atRate: 1.0)
                } else {
                    player.play()
                }

            } else {
            }
        } else {
            
        }
        isProcessingRequest = false
    }
    
    
    func btnTappedPlaylist(_ sender: AnyObject) {
    }
    
    func btnTappedShuffle(_ sender: AnyObject) {
        previousRandomIndex = currentSongIndex // flushing previous states
        isShuffleOn = !isShuffleOn!
        if isShuffleOn == true {
            isRepeatOn = false
        }
    }
    
    func btnTappedRewind(_ sender: AnyObject) {
    }
    
    func btnTappedPlay(_ sender: AnyObject) {
        if player.timeControlStatus == .playing {
            player.pause() // Pause the current music track

        } else if player.timeControlStatus == .waitingToPlayAtSpecifiedRate {
            
        } else if player.timeControlStatus == .paused {
            if player.currentItem != nil {
                if #available(iOS 10.0, *) {
                    player.playImmediately(atRate: 1.0)
                } else {
                    player.play()
                }
                

            } else {
//                playCurrentSong() // Play Current Music
                if self.playFirstSong == true {
                    self.playFirstSong = false
                    if (musicData?.song?.count)! > currentSongIndex {
                        currentSongIndex = 0
                        playCurrentSong()
                    }
                } else if (musicData?.song?.count)! > 0 {
                    currentSongIndex = 0
                    playCurrentSong()
                }

                //                playBtnOutlet.title = "Pause"
            }
        }
    }
    
    func btnTappedFastForward(_ sender: AnyObject) {
    }
    
    func btnTappedRepeat(_ sender: AnyObject) {
        isRepeatOn = !isRepeatOn!
        if isRepeatOn == true {
            isShuffleOn = false
        }
    }
    
    func musicSliderValueChanged(_ sender: AnyObject) {
        
    }
    
    @objc func playCurrentSong() {
        //Play Online
        isplayingVideo = false // rseting flag for remote controles of control center
        if self.musicData?.song != nil, (self.musicData?.song?.count)! > 0 , (self.musicData?.song?.count)! > currentSongIndex {
            self.setNowPlayingInfoCenterDetails()
        } else {

            MPNowPlayingInfoCenter.default().nowPlayingInfo = nil
        }
    }
    
    func addObserverToCurrentItem() {
        self.playerItem?.addObserver(self, forKeyPath: "status", options: .initial, context: nil)
        self.playerItem?.addObserver(self, forKeyPath: "rate", options: .initial, context: nil)
        self.playerItem?.addObserver(self, forKeyPath: "status", options: .new, context: nil)
        self.playerItem?.addObserver(self, forKeyPath: "rate", options: .new, context: nil)
    }
    
    func removeObserverOfCurrentItem() {
        self.playerItem?.removeObserver(self, forKeyPath: "status", context: nil)
        self.playerItem?.removeObserver(self, forKeyPath: "rate", context: nil)
        self.playerItem?.removeObserver(self, forKeyPath: "status", context: nil)
        self.playerItem?.removeObserver(self, forKeyPath: "rate", context: nil)
    }
    
    func audioPlayerDidFinishPlaying(player: AVAudioPlayer, successfully flag: Bool) {
        
    }
    
    func audioPlayerDecodeErrorDidOccur(player: AVAudioPlayer, error: NSError?) {
        
    }
    
    
}
