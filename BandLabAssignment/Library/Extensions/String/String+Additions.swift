//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

// MARK: - String Extension
extension String {

    /**
     Get height of string

     - parameter width: Max width of string to calculate height
     - parameter font:  Font of string

     - returns: Height of string
     */
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat? {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)

        return boundingBox.height
    }

    /**
     Get width of string

     - parameter width: Max width of string to calculate height
     - parameter font:  Font of string

     - returns: Height of string
     */
    func widthWithConstrainedHeight(_ height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)

        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)

        return ceil(boundingBox.width)
    }

    /**
     Get nsdata from string

     - returns: A NSdata from string
     */
    func toData() -> Data {

        return data(using: String.Encoding.utf8, allowLossyConversion: false)!
    }

    /**
     Returns an array of strings, each of which is a substring of self formed by splitting it on separator.

     - parameter separator: Character used to split the string
     - returns: Array of substrings
     */
    func explode(_ separator: Character) -> [String] {

        return characters.split(whereSeparator: { (element: Character) -> Bool in
            element == separator
        }).map { String($0) }
    }

    /**
     Specify that string contains only letters.

     - returns: A Bool return true if only letters otherwise false.
     */
    func containsOnlyLetters() -> Bool {

        for chr in characters {
            if !(chr >= "a" && chr <= "z") || !(chr >= "A" && chr <= "Z") {
                return false
            }
        }
        return true
    }

    /**
     Specify that string contains only number.

     - returns: A Bool return true if string has only letters otherwise false.
     */
    func containOnlyNumber() -> Bool {
        let numberFormat = "^[0-9]*$"
        let numberPredicate = NSPredicate(format: "SELF MATCHES %@", numberFormat)
        return numberPredicate.evaluate(with: self)
    }

    /**
     Get array from string

     - parameter seperator: String to seperate array

     - returns: Array from string
     */
    func toArray(_ seperator: String) -> [String] {

        return components(separatedBy: seperator)
    }

    /**
     Get substring in string.

     - returns: A Bool return true if string has substring otherwise false.
     */
    func containsSubstring() -> Bool {

        return contains(self)
    }
}
