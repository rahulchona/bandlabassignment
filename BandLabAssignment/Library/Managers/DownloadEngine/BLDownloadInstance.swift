//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import CoreGraphics


public class BLDownloadInstance : NSObject
{
    
    public typealias remainingDownloadTime = ((CGFloat) -> (Void))?
    public typealias currentProgressStat = ((CGFloat,BLDownloadInstance) -> (Void))?
    public typealias isDownloadCompleted = ((Bool, String?, Error?) -> (Void))?
    
    
    public var remainingTimeClosure : remainingDownloadTime
    public var currentProgressClosure : currentProgressStat
    public var downloadStatusClosure : isDownloadCompleted
    
    public var downloadTask : URLSessionDownloadTask
    public var filename : String
    public var friendlyName : String
    public var filePath : String
    public var downloadDate : Date
    
    
    public init(withDownloadTask : URLSessionDownloadTask, remainingTime :  remainingDownloadTime , progess :  currentProgressStat, status :  isDownloadCompleted,  name :  String, friendlyName :  String, path :  String, date :  Date ) {
        
        
        self.downloadTask = withDownloadTask
        self.remainingTimeClosure = remainingTime
        self.currentProgressClosure = progess
        self.downloadStatusClosure = status
        self.filename = name
        self.friendlyName = friendlyName
        self.filePath = path
        self.downloadDate = date
        
    }
    
    
    
    
}

