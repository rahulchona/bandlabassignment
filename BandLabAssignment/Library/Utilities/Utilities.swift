//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation

// class Utilities {

/**
 Global function to check if the input object is initialized or not.

 - parameter value: value to verify for initialization

 - returns: true if initialized
 */
func isObjectInitialized(_ value: AnyObject?) -> Bool {
    guard let _ = value else {
        return false
    }
    return true
}

// }
