//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIStoryboard Extension
extension UIStoryboard {

    /**
     Convenience Initializers to initialize storyboard.

     - parameter storyboard: String of storyboard name
     - parameter bundle:     NSBundle object

     - returns: A Storyboard object
     */
    convenience init(storyboard: String, bundle: Bundle? = nil) {
        self.init(name: storyboard, bundle: bundle)
    }

    /**
     Initiate view controller with view controller name.

     - returns: A UIView controller object
     */
    func instantiateViewController<T: UIViewController>() -> T {
        var fullName: String = NSStringFromClass(T.self)
        if let range = fullName.range(of: ".", options: .backwards) {
            fullName = fullName.substring(from: range.upperBound)
        }

        guard let viewController = self.instantiateViewController(withIdentifier: fullName) as? T else {
            fatalError("Couldn't instantiate view controller with identifier \(fullName) ")
        }

        return viewController
    }

    class func onboardingStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Onboarding", bundle: nil)
    }

    class func mainStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }

    class func registrationStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Registration", bundle: nil)
    }

    class func dashboardStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Dashboard", bundle: nil)
    }

    class func profileStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Profile", bundle: nil)
    }

    class func addressStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Address", bundle: nil)
    }

    class func paymentStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Payment", bundle: nil)
    }
    
    class func productStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Product", bundle: nil)
    }
    
    class func popupStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Popup", bundle: nil)
    }
    
    class func orderStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Order", bundle: nil)
    }
    
    class func notificationStoryboard() -> UIStoryboard {
        return UIStoryboard(name: "Notification", bundle: nil)
    }
}
