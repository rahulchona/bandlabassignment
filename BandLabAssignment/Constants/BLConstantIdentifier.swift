//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

enum StoryboardIdentifier {

    enum controller: String {
        case songListViewController = "BLSongListViewControllerID"
    }
}

enum SongState: Int {
    case NotDownloaded = 1
    case Downloading = 2
    case Playing = 3
    case Paused = 4
    case Downloaded = 5
}

// Definition:
extension Notification.Name {
    static let DownloadProgressNotification = Notification.Name("DownloadProgressNotification")
    static let DownloadCompletedNotification = Notification.Name("DownloadCompletedNotification")
    static let ReprocessSongsStateNotification = Notification.Name("ReprocessSongsState")
    static let UIApplicationDidBecomeActiveNotification = Notification.Name("UIApplicationDidBecomeActiveNotification")
    static let SongFinishedNotification = Notification.Name("SongFinished")
    static let SongDownloadFailedNotification = Notification.Name("SongDownloadFailed")
}
