//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UILabel Extension
extension UILabel {

    /**
     Override method of awake from nib to change font size as per aspect ratio.
     */
    open override func awakeFromNib() {

        super.awakeFromNib()

        if let font = self.font {

            let screenRatio = UIScreen.main.bounds.size.width / 375.0
            let fontSize = font.pointSize * screenRatio

            self.font = UIFont(name: font.fontName, size: fontSize)!
        }
    }
}

//MARK: - Attributes Extension
extension UILabel {

    func updateAttributes(firstText: String, firstColor: UIColor, firstFont: UIFont, secondText: String, secondColor: UIColor, secondFont: UIFont) {

        let firstAttributes = [NSAttributedStringKey.foregroundColor: firstColor, NSAttributedStringKey.font: firstFont]
        let secondAttributes = [NSAttributedStringKey.foregroundColor: secondColor, NSAttributedStringKey.font: secondFont]
        let first = NSMutableAttributedString(string: firstText, attributes: firstAttributes)
        let second = NSMutableAttributedString(string: secondText, attributes: secondAttributes)
        let combination = NSMutableAttributedString()
        combination.append(first)
        combination.append(second)
        self.attributedText = combination
    }
}
