//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
// MARK: - Character Extension
public extension Character {

    /**
     If the character represents an integer that fits into an Int, returns
     the corresponding integer.
     */
    public func toInt() -> Int? {
        return Int(String(self))
    }
}
