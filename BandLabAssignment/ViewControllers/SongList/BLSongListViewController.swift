//
//  SongListViewController.swift
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import UIKit
import MediaPlayer

class BLSongListViewController: UIViewController, UpdateSongListDelegate {
    
    //MARK: - VARIABLES -
    var currentTrack: SongData?
    var songsBase  : SongsListBaseClass?
    @objc var downloadTask: URLSessionDownloadTask?
    @objc var justBecameActive = false
    @objc var musicPlayer: AVPlayer!
    @objc var lastIndexPath : IndexPath! = IndexPath(item: 0, section: 0)
    @objc var firstTime = true
    @objc var newStation = true
    
    
    //MARK: - IBOUTLETS -
    @IBOutlet weak var songsTableView: UITableView!
    
    //MARK: - VIEWCONTROLLER LIFECYCLE METHODS -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        initializeItemListTableView()
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name:NSNotification.Name(rawValue: "UIApplicationDidBecomeActiveNotification"), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.AVAudioSessionInterruption, object: AVAudioSession.sharedInstance())
        NotificationCenter.default.removeObserver(self, name: .DownloadCompletedNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .ReprocessSongsStateNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .SongFinishedNotification, object: nil)
    }
}

//MARK: - UI RELATED -
extension BLSongListViewController {
    private func initializeItemListTableView() {
        //Register table
        self.songsTableView.estimatedRowHeight = 87.0
        self.songsTableView.rowHeight = UITableViewAutomaticDimension
        
        //Register nib
        BLSongTableViewCell.registerNib(forTable: self.songsTableView)
        
        //Setup Observor
        setupObservors()
        
        //API CALL
        self.callsongsListAPI()
    }
}

//MARK: - Notification Related -
extension BLSongListViewController {
    func setupObservors() {
        NotificationCenter.default.addObserver(self, selector: #selector(downloadCompleted(notification:)), name: .DownloadCompletedNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reprocessSongsState), name: .ReprocessSongsStateNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BLSongListViewController.songDidEndPlaying(notification:)), name: .SongFinishedNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(BLSongListViewController.songDidFailedToDownload(notification:)), name: .SongDownloadFailedNotification, object: nil)
    }
    
    @objc func downloadCompleted(notification: Notification) {
        //        DataManager.shared.processSongsWithStates(songsBaseModel: self.songsBase!)
        if (notification.userInfo?["SongUrl"] as? String) != nil && self.songsBase?.data != nil {
            for song in (self.songsBase?.data)! {
                if song.audioURL == (notification.userInfo?["SongUrl"] as! String) {
                    song.songState = SongState.Downloaded
                }
                
                if BLCacheManagement.fileExistForURL(url: URL(string: (song.audioURL!))!) {
                    if song.songState != .Playing {
                        song.songState = SongState.Downloaded
                    }
                }
                
            }
        }
        reloadSongListTableView()
    }
    
    @objc func reprocessSongsState() {
        DataManager.shared.reprocessSongsWithStates(songsBaseModel: self.songsBase!)
        reloadSongListTableView()
    }
    
    @objc func songDidEndPlaying(notification: Notification) {
        
        if (notification.userInfo?["SongUrl"] as? String) != nil && self.songsBase?.data != nil {
            for song in (self.songsBase?.data)! {
                if song.audioURL == (notification.userInfo?["SongUrl"] as! String) {
                    song.songState = SongState.Paused
                    MusicPlayerManager.shared.player.seek(to: kCMTimeZero)
                    MusicPlayerManager.shared.player.pause()
                    break
                }
            }
        }
        
        reloadSongListTableView()
    }
    
    @objc func songDidFailedToDownload(notification: Notification) {
        
        if (notification.userInfo?["SongUrl"] as? String) != nil && self.songsBase?.data != nil {
            for song in (self.songsBase?.data)! {
                if song.audioURL == (notification.userInfo?["SongUrl"] as! String) {
                    song.songState = SongState.NotDownloaded
                    break
                }
            }
        }
        
        reloadSongListTableView()
    }
    
    func reloadSongListTableView() {
        DispatchQueue.main.async {
            self.songsTableView.reloadData()
        }
    }
}

//MARK: - NETWORK RELATED -
extension BLSongListViewController {

    func callsongsListAPI() {

        DataManager.shared.getSongsListAPI() { [weak self] (status, error, message, songsBase) in

            if status {
                self?.songsBase = songsBase
                self?.songsTableView.reloadData()
            } else {
            }
        }
    }
}

