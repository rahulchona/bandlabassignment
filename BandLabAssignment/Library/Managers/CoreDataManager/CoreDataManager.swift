//
//  BandLabAssignment
//
//  Created by Rahul Chona on 21/04/18.
//  Copyright © 2018 Rahul Chona. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataManager: NSObject {
    
    //creating context models and persitent store
    var context : NSManagedObjectContext?
    
    //creating singleton object
    static let sharedInstance = CoreDataManager()
    
    //override private init method
    private override init() {
        super.init ()
        context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }
    
    func fetchOfflineResource(with name: String? = "") -> OfflineSongs? {
        return OfflineSongs.fetchOfflineResourceByName(offlineResourceName: name!) ?? nil
    }
    
    func fetchAllRecords() -> [Any] {
        let offlineResourcesFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "OfflineSongs")
        do {
            return try self.context!.fetch(offlineResourcesFetch)
        } catch {
            fatalError("Failed to fetch records: \(error)")
        }
    }

    func fetchAllSongsOfflineResources() -> [Any] {
        if let _ = OfflineSongs.fetchSongsOfflineResources() {
            return OfflineSongs.fetchSongsOfflineResources()!
        }
        return []
    }
    
    func fetchDataForEntity(entity: NSString, predicate: NSPredicate?, sortWith: NSArray?) ->
        (NSArray?) {
            return nil
    }
    
    func deleteAll() {
        OfflineSongs.deleteAllOfflineResourceRecords()
    }
    
    func updateRecord(offlineResourceDetails: [String : AnyObject]) -> Bool {
        return OfflineSongs.updateOfflineResource(offlineResourceDetails: offlineResourceDetails)
    }
    
    func saveOfflineResourceRecord(offlineResourceDetails: [String : AnyObject]) -> Bool {
        return OfflineSongs.saveOfflineResource(offlineResourceDetails: offlineResourceDetails)
    }
    
    func deleteOfflineResourceRecord(fileName: String) -> Bool {
        return OfflineSongs.deleteOfflineResource(fileName: fileName)
    }
    
    func updateDownloadStatus(fileName: String) -> Bool {
        return OfflineSongs.updateDownloadStatus(fileName: fileName)
    }
}

